import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CargaArchivoProvider, ArchivoSubir } from '../../providers/carga-archivo/carga-archivo';
import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker';




@Component({
  selector: 'page-subir',
  templateUrl: 'subir.html',
})
export class SubirPage {
  titulo:string="";
  imagenPreviw:string="";
  image64:string;

  constructor(private viewCtrl:ViewController,private camera: Camera,
    public cargarArchivo:CargaArchivoProvider,private imagePicker: ImagePicker) {
  }


  cerrar_modal(){
    this.viewCtrl.dismiss();
  }


  mostrarCamara(){

    const options: CameraOptions = {
      quality: 50,
      sourceType:this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    

    this.camera.getPicture(options).then((imageData) => {
      this.imagenPreviw = 'data:image/jpeg;base64,' + imageData;
      this.image64 = this.imagenPreviw;
    }, (err) => {
     console.log(JSON.stringify(err))
    });

    this.camera.getPicture(options).then(()=>this.cargarArchivo.showToast("imagen tomada"));

    
  }

  _imagePicker(){

    let options:ImagePickerOptions={
      maximumImagesCount:1,
      outputType:1,
      quality:70
    };

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imagenPreviw = 'data:image/jpeg;base64,' + results[i];
        this.image64 = results[i];
      }
    }, (err) => {
        this.cargarArchivo.showToast("Error al cargar la imagen..",50000);
     });

  }


  cargar_post(){

    let archivo: ArchivoSubir ={
      titulo:this.titulo,
      img:this.image64
    }
    

    this.cargarArchivo.cargarImagen_firebase(archivo).then(()=>this.cerrar_modal());

  }


}
